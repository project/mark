<?php
foreach ($links as $link):
?>
<div class="mark-link <?php print ($link->marked ? 'marked' : 'unmarked') ?>">
  <?php print l($link->title, $link->path, $link->options) ?>
</div>
<?php endforeach; ?>
